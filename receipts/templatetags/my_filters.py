from django import template

register = template.Library()

# IMPORTANT: IF USING THESE FILTERS IN A LOOP:
# THE ARGUMENTS MUST MATCH IN THE TEMPLATE!!!!!
# Eg.: the loop must start with:
# for cat in category
# OR:
# for acct in accounts


# Category list: take receipts, filter them by category,
# then return length (count) of receipts by category
# need args: receipt(aka variable) and cat(aka value of arg)
@register.filter(name="cat_receipts")
def cat_receipts_count(rec, cat):
    return len(rec.filter(category=cat))


# Account: take receipts, filter them by account, then return length of
# said filter as num of receipts by specific account
# need args: receipt (aka variable) and acct(aka value of arg)
@register.filter(name="acct_receipts")
def acct_receipts_count(rec, acct):
    return len(rec.filter(account=acct))
