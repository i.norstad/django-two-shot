from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


# To be used with the view 'create_receipt'
class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ("vendor", "total", "tax", "date", "category", "account")


# To be used with the view 'create_category'
class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


# To be used with view 'create_account'
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
