from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


# Create receipt
@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


# Show list of receipts
@login_required(login_url="/accounts/login/")
def receipt_list(request):
    # add filter to only display receipts the belong to logged in user
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


# Create a list of categories
@login_required(login_url="/accounts/login/")
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {"category": category, "receipts": receipts}
    return render(request, "receipts/category_list.html", context)


# Create a list of accounts
@login_required(login_url="/accounts/login/")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {"accounts": accounts, "receipts": receipts}
    return render(request, "receipts/account_list.html", context)


# Create a new category
@login_required(login_url="/accounts/login/")
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


# Create Account:
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
